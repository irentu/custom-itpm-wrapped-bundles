CDL FLEX Wrapped Pellet Reasoner
================================

Wrapped version of the Pellet Reasoner for OWL. The bundle 
also includes references to Jena because SPARQL and
SPARQLDL are part of the features. Unfortunately version
0.8 of TDB is included, which leads to unresolvable
incompatibilities with newer versions of the Jena API.
Due to this problem, Jena and OWLAPI-SPARQL cannot be
packaged into one project.